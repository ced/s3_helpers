#!/usr/bin/env python3
"""
Replay an strace output file's reads and writes, captured like this:

strace -ttt -xx -s 2096640 -o/mnt/storage/strace.log -P/mnt/casa_data/2013.1.00187.S/science_goal.uid___A001_X145_Xb2/group.uid___A001_X145_Xb3/member.uid___A001_X145_Xb4/calibrated/working/uid___A002_X9aca45_X14a3.ms/table.f24_TSM4 casa --nogui --pipeline --logfile /mnt/storage/casa.log -c scriptForPI.py
"""

import argparse
import io
import os
import re

OP_REGEX = re.compile(r'^(?P<timestamp>\d+\.\d+)\s+(?P<operation>\w+)\((?P<args>.*?)\)')

def parse_open_args(args):
    "Return a dict of args for POSIX open()"
    flags_map = {'O_RDONLY':    'rb',
                 'O_WRONLY':    'wb',
                 'O_RDWR':      'r+b', # w+b opens and truncates to 0 bytes. r+b opens without truncation
                 }

    arg_names = ('path', 'flags', 'mode_t', 'mode')
    arg_dict = dict(zip(arg_names[:len(args)], args))
    arg_dict['path'] = bytes.fromhex(arg_dict['path'].strip('"').replace('\\x', '')).decode('utf-8')

    flag_list = arg_dict['flags'].split('|')
    for key in flags_map:
        if key in flag_list:
            mode_string = flags_map[key]
    if 'O_RDWR' in flag_list and 'O_TRUNC' in flag_list:
        mode_string = 'w+b'
    if 'O_APPEND' in flag_list:
        mode_string += 'a'
    arg_dict['mode_string'] = mode_string
    
    if 'mode_t' in arg_dict:
        arg_dict['mode_t'] = int(arg_dict['mode_t'], 8)
    return arg_dict

def parse_lseek_args(args):
    "Return a dict of args for POSIX lseek()"
    whence_map = {'SEEK_SET': io.SEEK_SET,
                  'SEEK_CUR': io.SEEK_CUR,
                  'SEEK_END': io.SEEK_END}

    arg_names = ('fd', 'offset', 'whence')
    arg_dict = dict(zip(arg_names[:len(args)], args))
    arg_dict['fd'] = int(arg_dict['fd'])
    arg_dict['offset'] = int(arg_dict['offset'])
    if 'whence' in arg_dict:
        arg_dict['whence'] = whence_map[arg_dict['whence']]
    return arg_dict

def parse_write_args(args):
    "Return a dict of args for POSIX write()"
    arg_names = ('fd', 'buffer', 'count')
    arg_dict = dict(zip(arg_names[:len(args)], args))
    arg_dict['fd'] = int(arg_dict['fd'])
    arg_dict['count'] = int(arg_dict['count'])
    arg_dict['buffer'] = bytes.fromhex(arg_dict['buffer'].strip('"').replace('\\x', ''))
    return arg_dict

def parse_read_args(args):
    "Return a dict of args for POSIX read()"
    arg_names = ('fd', 'buffer', 'count')
    arg_dict = dict(zip(arg_names[:len(args)], args))
    arg_dict['fd'] = int(arg_dict['fd'])
    arg_dict['count'] = int(arg_dict['count'])
    return arg_dict

def parse_close_args(args):
    "Return a dict of args for POSIX close()"
    return {'fd': int(args[0])}

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Replay strace output back to filesystem")
    parser.add_argument('-i', '--input',
                        type=argparse.FileType('r'),
                        required=True,
                        help='strace output file to replay')
    parser.add_argument('-d', '--dir',
                        help='Replay to a directory (mutually exlusive with --output parameter)')
    parser.add_argument('-o', '--output',
                        help='Replay to a file (mutually exclusive with --dir parameter)')
    # Parse cli args
    cli_kwargs = vars(parser.parse_args())

    if cli_kwargs['dir'] and cli_kwargs['output']:
        raise RuntimeError('--dir and --output parameters are mutually exclusive! Pick one only please.')
    if not cli_kwargs['dir'] and not cli_kwargs['output']:
        raise RuntimeError('--dir or --output parameters need to be specified! Pick one please.')

    skip_ops = ('fcntl',)
    op_arg_parsers = {'open':   parse_open_args,
                      'lseek':  parse_lseek_args,
                      'write':  parse_write_args,
                      'read':   parse_read_args,
                      'close':  parse_close_args,
                     }

    output_filehandles = {}
    fd_to_name = {}

    line_num = 0
    for line in cli_kwargs['input']:
        line_num += 1
        line = line.strip()
        match = OP_REGEX.match(line)
        if not match:
            print('no match at line {num}: {text}'.format(num=line_num, text=line))
            continue
        if match.group('operation') in skip_ops:
            print('skipping {op} at line {num}: {text}'.format(op=match.group('operation'), num=line_num, text=line))
            continue

        if match.group('operation') in op_arg_parsers:
            args = op_arg_parsers[match.group('operation')](match.group('args').split(', '))
            try:
                if match.group('operation') == 'open':
                    dest_fd = int(line.rpartition(' = ')[2])
                    if cli_kwargs['dir']:
                        dest_name = os.path.join(cli_kwargs['dir'], args['path'].lstrip('/'))
                    else:
                        dest_name = cli_kwargs['output']
                    print('Opening {} as {}'.format(dest_name, args['mode_string']))
                    if os.path.dirname(dest_name):
                        os.makedirs(os.path.dirname(dest_name), exist_ok=True)
                    output_filehandles[dest_fd] = open(dest_name, args['mode_string'])
                    if 'mode_t' in args:
                        os.chmod(dest_name, args['mode_t'])
                    fd_to_name[dest_fd] = dest_name
                elif match.group('operation') == 'lseek':
                    #print('seek to {offset} {whence}'.format(offset=args['offset'], whence=args['whence']))
                    output_filehandles[args['fd']].seek(args['offset'], args['whence'])
                elif match.group('operation') == 'write':
                    output_filehandles[args['fd']].write(args['buffer'][:args['count']])
                    output_filehandles[args['fd']].flush()
                    #print('wrote {num} bytes'.format(num=len(args['buffer'][:args['count']])))
                elif match.group('operation') == 'read':
                    output_filehandles[args['fd']].read(args['count'])
                    #print('read {num} bytes'.format(num=args['count']))
                elif match.group('operation') == 'close':
                    print('Closing {}'.format(fd_to_name[args['fd']]))
                    output_filehandles[args['fd']].close()
            except Exception as err:
                print('error at line {num} for operation {op}'.format(num=line_num, op=match.group('operation')))
                raise err
        else:
            print(line)
            raise RuntimeError("I don't know how to process operation {op} at line {num}!".format(op=match.group('operation'), num=line_num))
