#!/usr/bin/env python3
"A test suite, to probe DDN WOS S3 replication functionality"

import base64
import hashlib
import logging
import random
import string
import sys
import unittest
from pprint import pprint

import boto3
import botocore
from botocore.awsrequest import AWSRequest
import requests

LOG = logging.getLogger('wos_test_replication')
LOG.level = logging.INFO
SH = logging.StreamHandler(sys.stdout)
LOG.addHandler(SH)

SITE_ENDPOINTS = {
    'site-a': 'https://site-a-endpoint-here',
    'site-b': 'https://site-b-endpoint-here',
}

S3_TAG = "c3tp_wos_testsuite-"

def random_string(length=13):
    "Return a random string of given length"
    random.seed()
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])

def s3_bucket_name(prefix='c3tp-'):
    """
    Return a valid S3 bucket name, that follows AWS conventions
    (http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html)
    """
    min_length = 3
    max_length = 63 - len(prefix)
    return prefix + random_string(random.randint(min_length, max_length)).lower()

def tree_to_xml(tree):
    """
    Return an xml string representing the tree of element pairs.
    The tree looks like: [(tag, value), ...]
    where value could be a string or tree.
    """
    text = ''
    for (tag, value) in tree:
        text += '<' + tag + '>'
        if isinstance(value, (list, tuple)):
            text += tree_to_xml(value)
        else:
            text += str(value)
        text += '</' + tag + '>'

    return text

def get_s3_config():
    "Return the S3 config (access/secret access key) available in boto config."
    session = botocore.session.Session()
    if not session.profile in session.full_config['profiles']:
        return session.full_config['profiles']['default']
    return session.full_config['profiles'][session.profile]

class TestWOSBucketReplication(unittest.TestCase):
    "Test WOS S3 Bucket replication functionality"

    # This ARN template is different from AWS ARN format:
    # http://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html
    # arn:partition:service:region:account-id:resource
    # arn:partition:service:region:account-id:resourcetype/resource
    # arn:partition:service:region:account-id:resourcetype:resource#
    # AWS S3 arn would normally look like:
    # arn:aws:s3:::bucket_name
    WOS_ARN_TEMPLATE = '{site}:::{bucket}'

    @classmethod
    def setUpClass(self):
        self.s3_clients = {region: boto3.client('s3', endpoint_url=endpoint, config=botocore.config.Config(signature_version='s3'))
                           for (region, endpoint) in SITE_ENDPOINTS.items()}
        bucket_name = s3_bucket_name()
        self.s3_buckets = {region: s3_bucket_name() for region in SITE_ENDPOINTS}
        # NOTE(cedric): Identical bucket names trigger a WOS S3 bug (case 87330)
        #self.s3_buckets = {region: bucket_name for region in SITE_ENDPOINTS}
        self.targets = dict(zip(('source', 'dest'), SITE_ENDPOINTS))

        self.replication_configured = False

        for (direction, name) in self.targets.items():
            LOG.debug("{direction} is {region}:{bucket}".format(direction=direction.capitalize(), region=name, bucket=self.s3_buckets[name]))

        for (region, name) in self.s3_buckets.items():
            self.s3_clients[region].create_bucket(Bucket=name)
            # Enable object versioning
            self.s3_clients[region].put_bucket_versioning(Bucket=name,
                                                          VersioningConfiguration={'Status': 'Enabled'})

    @classmethod
    def tearDownClass(self):
        for (region, name) in self.s3_buckets.items():
            self.s3_clients[region].delete_bucket(Bucket=name)

    # NOTE(cedric): This test is expected to fail in WOS S3 v2.3,
    # because it does not support Role, Prefix parameters in API call.
    @unittest.expectedFailure
    def test_1_configure_replication(self):
        "configure bucket replication"
        wos_arn = self.WOS_ARN_TEMPLATE.format(site=self.targets['dest'],
                                               bucket=self.s3_buckets[self.targets['dest']])
        rule_id = S3_TAG + random_string()
        resp = self.s3_clients[self.targets['source']].put_bucket_replication(
            Bucket=self.s3_buckets[self.targets['source']],
            ReplicationConfiguration={
                'Rules': [
                    {'ID':          rule_id,
                     'Status':      'Enabled',
                     'Destination': {'Bucket': wos_arn},
                    },
                ],
                })
        self.replication_configured = True

    # NOTE(cedric): This uses the WOS S3 custom API call for configuring bucket replication.
    def test_2_configure_replication_wos(self):
        "configure bucket replication with custom XML payload"
        if self.replication_configured:
            self.skipTest('Replication config supported via boto3 API call!')

        rule_id = S3_TAG + random_string()

        # We'll test WOS S3 ability to return bucket location info,
        # instead of using the region name we started with.
        dest_location_resp = self.s3_clients[self.targets['dest']].get_bucket_location(Bucket=self.s3_buckets[self.targets['dest']])
        dest_region = dest_location_resp['LocationConstraint']
        self.assertEqual(dest_region, self.targets['dest'], 'destination region mismatch!')

        wos_arn = self.WOS_ARN_TEMPLATE.format(site=dest_region,
                                               bucket=self.s3_buckets[self.targets['dest']])
        tree = ('ReplicationConfiguration',
                    (('Rule',
                        (('ID', rule_id),
                         ('Status', 'Enabled'),
                         ('Destination',
                             (('Bucket', wos_arn),)))),))
        xml = tree_to_xml([tree])

        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}?replication'.format(endpoint=SITE_ENDPOINTS[self.targets['source']],
                                                       bucket=self.s3_buckets[self.targets['source']])
        aws_req = AWSRequest()
        aws_req.method = 'PUT'
        aws_req.url = url
        aws_req.data = xml.encode('utf-8')
        aws_req.headers['Content-MD5'] = base64.standard_b64encode(hashlib.md5(aws_req.data).digest()).decode('utf-8')

        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(aws_req)
        prep_req = aws_req.prepare()

        print()
        print(self.targets)
        print(self.s3_buckets)
        print('S3 request:')
        print(url)
        print(aws_req.headers)
        print(aws_req.body.decode('utf-8'))

        resp = requests.put(url, headers=aws_req.headers, data=aws_req.body, verify=False)

        print('S3 response')
        print(resp.text)
        print('source bucket location')
        pprint(self.s3_clients[self.targets['source']].get_bucket_location(Bucket=self.s3_buckets[self.targets['source']]))
        print('dest bucket location')
        pprint(self.s3_clients[self.targets['dest']].get_bucket_location(Bucket=self.s3_buckets[self.targets['dest']]))

        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()
        self.replication_configured = True

    def test_3_object_replication(self):
        self.fail('TODO')

if __name__ == "__main__":
    unittest.main(verbosity=2)
