#!/usr/bin/env python
"""
Test bucket and object replication within a WOS S3 cluster.
"""

import os.path
import random
import string
import subprocess
import sys
import time

import boto3
import botocore
import botocore.config

DEFAULT_S3_HTTP_PORT = 8080
S3_ENDPOINT_TEMPLATE = 'http://{host}:{port}'

def get_s3_config():
    """
    Return the S3 config (access/secret access key) available in boto config.
    This can be used when it's necessary to save the same information in another
    config file (like minio, minfs, etc)
    """
    session = botocore.session.Session()
    if not session.profile in session.full_config['profiles']:
        return session.full_config['profiles']['default']
    return session.full_config['profiles'][session.profile]

def random_string(length=13):
    "Return a random string of given length"
    random.seed()
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])

def s3_bucket_name(prefix='c3tp-'):
    """
    Return a valid S3 bucket name, that follows AWS conventions
    (http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html)
    """
    min_length = 3
    max_length = 63 - len(prefix)
    return prefix + random_string(random.randint(min_length, max_length)).lower()

def open_ssh(host, cmd_args):
    "Return a subprocess.Popen object for an ssh session to the host"
    return subprocess.Popen(['/usr/bin/ssh',
                             '-o', 'StrictHostKeyChecking=no',
                             '-o', 'GlobalKnownHostsFile=/dev/null',
                             '-o', 'UserKnownHostsFile=/dev/null',
                             '-q',
                             host] + cmd_args,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE)

def master(slaves):
    """
    As a master process, start remote copies of this script in slave mode,
    and have them report back how long it took for Bucket/Object to replicate to them.
    """
    # We'll pipe ourselves into ssh sessions, to run slaves remotely
    this_file = os.path.realpath(__file__)
    file_contents = open(this_file, 'r').read()

    # s3_config = {aws_access_key_id, aws_secret_access_key, ...}
    s3_config = get_s3_config()

    bucket_name = s3_bucket_name()
    print('Bucket name is {0}'.format(bucket_name))

    s3_clients = {}
    ssh_sessions = {}
    for host in slaves:
        url = S3_ENDPOINT_TEMPLATE.format(host=host, port=DEFAULT_S3_HTTP_PORT)
        s3_clients[host] = boto3.client('s3',
                                        endpoint_url=url,
                                        config=botocore.config.Config(signature_version='s3'))
        print('Starting SSH session to {host}'.format(host=host))
        ssh_sessions[host] = open_ssh(host, ['python', '-',
                                             'slave',
                                             host,
                                             bucket_name,
                                             s3_config['aws_access_key_id'],
                                             s3_config['aws_secret_access_key']])
        # Start slave script on remote host through STDIN pipe
        ssh_sessions[host].stdin.write(file_contents)
        ssh_sessions[host].stdin.close()

    # Create bucket on randomly-chosen S3 node
    pick = random.choice(slaves)
    print('Creating bucket on {host}'.format(host=pick))
    result = s3_clients[pick].create_bucket(Bucket=bucket_name)
    start_time = time.time()

    if result['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise RuntimeError('Bucket creation failed!')
    print('Create time: {ts}'.format(ts=start_time))

    # Collect output from slaves
    slave_output = {}
    for host in slaves:
        slave_output[host] = ssh_sessions[host].stdout.read().decode('utf-8')
        slave_replication_time = float(slave_output[host])
        slave_replication_delta = slave_replication_time - start_time
        print('host {host} latency {delta} seconds'.format(host=host, delta=slave_replication_delta))
    print('Removing bucket {0}'.format(bucket_name))
    s3_clients[pick].delete_bucket(Bucket=bucket_name)

def slave(host, bucket_name, access_key, secret_access_key):
    """
    As a slave process, poll local S3 instance repeatedly for the given Bucket/Object,
    and print out when it showed up.
    """
    url = S3_ENDPOINT_TEMPLATE.format(host=host, port=DEFAULT_S3_HTTP_PORT)
    s3_client = boto3.client('s3',
                             endpoint_url=url,
                             aws_access_key_id=access_key,
                             aws_secret_access_key=secret_access_key,
                             config=botocore.config.Config(signature_version='s3'))

    current_time = None
    while True:
        try:
            current_time = time.time()
            result = s3_client.head_bucket(Bucket=bucket_name)
        except Exception as err:
            time.sleep(0.005)
        else:
            break
    print(current_time)

if __name__ == '__main__':
    # script master [slave_node1, slave_node2, ...]
    # script slave <node> <bucket_name> <access key> <secret access key>
    usage_text = 'Usage:\n{prog} master [slave_node1, slave_node2, ...]\n{prog} slave <node> <bucket_name> <access key> <secret access key>'.format(prog=sys.argv[0])
    if len(sys.argv) < 2 or sys.argv[1] not in ('master', 'slave'):
        print(usage_text)
        sys.exit(1)
    elif sys.argv[1] == 'master':
        master(slaves=sys.argv[2:])
    elif sys.argv[1] == 'slave':
        slave(*sys.argv[2:])

