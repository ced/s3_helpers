#!/usr/bin/env python
"""
Test read-time at beginning/middle/end of objects, for an S3 endpoint.
"""
import json, math, os, random, tempfile, time
from collections import Counter

import boto3
import botocore

import bokeh.layouts
from bokeh.charts import Bar, Scatter, output_file, show

S3_ENDPOINT = 'http://127.0.0.1:9000' # Minio
S3_BUCKET = 'rand-pos-test'
TEMP_DIR = '/tmp/rand_pos_test' # Where locally-generated objects go

TEST_COUNT = 250
STATS_OUTPUT = 'rand_pos_test_stats.json'

UNIT_MULTIPLIERS = {
    'byte':         1,
    'kilobyte':     1000,
    'kB':           1000,
    'kibibyte':     1024,
    'KiB':          1024,
    'KB':           1024,
    'megabyte':     1000**2,
    'MB':           1000**2,
    'mebibyte':     1024**2,
    'MiB':          1024**2,
    'gigabyte':     1000**3,
    'GB':           1000**3,
    'gibibyte':     1024**3,
    'GiB':          1024**3,
    'terabyte':     1000**4,
    'TB':           1000**4,
    'tebibyte':     1024**4,
    'TiB':          1024**4,
    'petabyte':     1000**5,
    'PB':           1000**5,
    'pebibyte':     1024**5,
    'PiB':          1024**5,
    'exabyte':      1000**6,
    'EB':           1000**6,
    'exbibyte':     1024**6,
    'EiB':          1024**6,
    'zettabyte':    1000**7,
    'ZB':           1000**7,
    'zebibyte':     1024**7,
    'ZiB':          1024**7,
    'yottabyte':    1000**8,
    'YB':           1000**8,
    'yobibyte':     1024**8,
    'YiB':          1024**8,
}

def gen_file(path, min_size=200, max_size=400, aligned=False):
    "Generate a file between min and max sizes in MB if not aligned, MiB if aligned"
    if aligned:
        unit = 'MiB'
    else:
        unit = 'MB'

    file_size = random.randint(min_size * UNIT_MULTIPLIERS[unit],
                               max_size * UNIT_MULTIPLIERS[unit])

    with tempfile.NamedTemporaryFile(dir=path, delete=False) as fh:
        fh.write(os.urandom(file_size))
    return fh.name

def gen_files(count=TEST_COUNT, aligned=False, base_path=TEMP_DIR):
    "Generate count test files (implicitly 200-400 MB, or MiB if aligned"

    if aligned:
        path = os.path.join(base_path, 'aligned')
    else:
        path = os.path.join(base_path, 'unaligned')

    os.makedirs(path, exist_ok=True)
    files = [os.path.join(path, f) for f in os.listdir(path)]

    while len(files) < count:
        name = gen_file(path, aligned=aligned)
        print('Generated ' + name)
        files.append(os.path.join(path, name))

    return files

def list_objects(paginator, bucket, prefix=None):
    "Return a list of available s3 object keys"
    keys = []
    for result in paginator.paginate(Bucket=bucket, Prefix=prefix):
        if not 'Contents' in result:
            continue
        keys += [o['Key'] for o in result['Contents']]
    return keys

def content_range_dict(text):
    """
    Decode an RFC2616 Content-Range field returned in an HTTP(S) response, and
    return a dictionary representing it.
    https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html

    Ex:
    content_range_dict('bytes 11000000-11610018/11610019')
    {'instance_length': 11610019, 'unit': 'bytes', 'last_byte_pos': 11610018, 'first_byte_pos': 11000000}

    In S3 API, instance_length seems to represent the total size of the object.
    """
    fields = {}
    (bytes_unit, _, range_spec) = text.partition(' ')
    fields['unit'] = bytes_unit
    if not '/' in range_spec:
        fields['instance_length'] = 'unknown'
        (first_byte_pos, _, last_byte_pos) = range_spec.partition('-')
        fields['first_byte_pos'] = int(first_byte_pos)
        fields['last_byte_pos'] = int(last_byte_pos)
    else:
        (byte_range_spec, _, instance_length) = range_spec.partition('/')
        (first_byte_pos, _, last_byte_pos) = byte_range_spec.partition('-')
        if instance_length == '*':
            fields['instance_length'] = 'unknown'
        else:
            fields['instance_length'] = int(instance_length)
        fields['first_byte_pos'] = int(first_byte_pos)
        fields['last_byte_pos'] = int(last_byte_pos)
    return fields

def s3_read_range(s3_obj, start=0, end=None):
    "Return data range from S3 object"
    # TODO(cedric): Split reads into 5GB chunks and concatenate the body,
    # if range of data is greater.
    if end is None or end > s3_obj.content_length:
        end = s3_obj.content_length

    if start == 0 and end == s3_obj.content_length:
        resp = s3_obj.get()
        return (start, end, resp['Body'].read())

    byte_range = 'bytes={start}-{end}'.format(start=start, end=end)
    resp = s3_obj.get(Range=byte_range)
    if 'ContentRange' in resp:
        content_range = content_range_dict(resp['ContentRange'])
        resp_start = content_range['first_byte_pos']
        resp_end = content_range['last_byte_pos']
    elif resp['ContentLength'] == s3_obj.content_length:
        resp_start = 0
        resp_end = s3_obj.content_length
    else:
        resp_start = start
        resp_end = start + resp['ContentLength']
    return (resp_start, resp_end, resp['Body'].read())

def pos_read(s3_obj, size):
    "Return results of position reads on the given object"
    read_times = []

    position_names = ['beginning', 'middle', 'end']
    longest_read_names = ['shortest', 'middle', 'longest']

    middle_pos = int(math.floor(s3_obj.content_length / float(2))) - size
    pos_indices = (0, middle_pos, s3_obj.content_length - (size + 1))

    for (pos_name, pos) in zip(position_names, pos_indices):
        start_time = time.time()
        (data_start, data_end, data) = s3_read_range(s3_obj, pos, pos + size)
        end_time = time.time()
        read_times.append({'position':  pos_name,
                           'read_time': end_time - start_time,
                           'read_size': len(data),
                           'obj_size':  s3_obj.content_length})

    short_to_long_reads = sorted(read_times, key=lambda r: r['read_time']) 
    for (place, read) in zip(longest_read_names, short_to_long_reads):
        read['read_time_placement'] = place

    return read_times

if __name__ == '__main__':
    s3_client = boto3.client('s3', endpoint_url=S3_ENDPOINT)
    s3 = boto3.resource('s3', endpoint_url=S3_ENDPOINT)
    paginator = s3_client.get_paginator('list_objects')

    # Check S3 bucket
    try:
        s3_client.head_bucket(Bucket=S3_BUCKET)
    except botocore.exceptions.ClientError as err:
        if err.response['Error']['Message'] == 'Not Found':
            print('Creating bucket {}'.format(S3_BUCKET))
            s3_client.create_bucket(Bucket=S3_BUCKET)
        else:
            raise

    # Generate objects
    unaligned_files = gen_files(count=10, aligned=False)  # multiple of 1000 ** 2
    aligned_files = gen_files(count=10, aligned=True)     # multiple of 1024 ** 2

    # Upload objects
    for (prefix, objects) in (('unaligned', unaligned_files), ('aligned', aligned_files)):
        for file_name in objects:
            obj_name = os.path.basename(file_name)
            key = os.path.join(prefix, obj_name)
            try:
                s3_obj = s3.Object(S3_BUCKET, key)
                s3_obj.reload()
            except botocore.exceptions.ClientError as err:
                if err.response['Error']['Message'] == 'Not Found':
                    print('Uploading {fn} to {b}:{o}'.format(fn=file_name, b=S3_BUCKET, o=key))
                    s3_obj.upload_file(file_name)
                else:
                    raise

    # List objects (in case there were more already in the bucket)
    unaligned_objects = list_objects(paginator, S3_BUCKET, 'unaligned')
    aligned_objects = list_objects(paginator, S3_BUCKET, 'aligned')

    # Perform position reads on randomly-chosen objects
    # All reads will be between 1-20MB
    read_units = 'MB'
    read_size = random.randint(1  * UNIT_MULTIPLIERS[read_units],
                               20 * UNIT_MULTIPLIERS[read_units])
    print('Read size will be {s} {u}'.format(s=read_size / float(UNIT_MULTIPLIERS[read_units]), u=read_units))
    unaligned_reads = []
    aligned_reads = []
    reads = 0
    while reads < TEST_COUNT:
        unaligned_pick = random.choice(unaligned_objects)
        aligned_pick = random.choice(aligned_objects)

        unaligned_obj = s3.Object(S3_BUCKET, unaligned_pick) 
        aligned_obj = s3.Object(S3_BUCKET, aligned_pick) 

        unaligned_reads.append(pos_read(unaligned_obj, read_size))
        aligned_reads.append(pos_read(aligned_obj, read_size))

        reads += 1
        print('read: {count}/{total}'.format(count=reads, total=TEST_COUNT))

    # Write read stats out
    stats = {'unaligned': unaligned_reads,
             'aligned':   aligned_reads}

    with open(STATS_OUTPUT, 'w') as stats_out:
        json.dump(stats, stats_out, indent=2, sort_keys=True)

    # Generate read stats graphs
    scatter_graphs = []
    bar_graphs = []
    for (alignment, reads) in stats.items():
        # Object read times by position in file
        scatter_reads = []
        for read in reads:
            scatter_reads.extend(read)

        scatter = Scatter(scatter_reads,
                          title='{al} S3 object size vs read time by position in file'.format(al=alignment),
                          x='obj_size',
                          y='read_time',
                          xlabel='Object size (in bytes)',
                          ylabel='Read time (in seconds)',
                          color='position',
                          marker='position',
                          legend='top_left')
        scatter_graphs.append(scatter)

        # Which reads took longest, by position in file
        bar = Bar(scatter_reads,
                  title='{al} longest reads by position in file'.format(al=alignment),
                  label='position',
                  stack='read_time_placement',
                  legend='top_right',
                  )
        bar_graphs.append(bar)

        # Percentile values of the same
        placements = {}
        for read in reads:
            for pos_read in read:
                if not pos_read['position'] in placements:
                    placements[pos_read['position']] = Counter()
                placements[pos_read['position']][pos_read['read_time_placement']] += 1

        for (position, cnt) in placements.items():
            for (place, count) in cnt.items():
                print('{al} {pos} {place} {pct}%'.format(al=alignment, pos=position, place=place, pct=(count / float(len(reads))) * 100))

    output_file('rand_pos_test_results.html'.format(al=alignment))
    show(bokeh.layouts.layout(scatter_graphs, bar_graphs))

