#!/usr/bin/env python3

import base64
import hashlib
import io
import logging
import math
import os
import random
import re
import string
import sys
import unittest
import warnings

import requests

import boto3
import botocore
import botocore.config
from boto3.s3.transfer import TransferConfig
from botocore.awsrequest import AWSRequest
from botocore.exceptions import ClientError
from botocore.vendored.requests.packages.urllib3.connectionpool import InsecureRequestWarning

warnings.filterwarnings('ignore', category=InsecureRequestWarning)

LOG = logging.getLogger('wos_test')
LOG.level = logging.INFO
#LOG.level = logging.DEBUG
SH = logging.StreamHandler(sys.stdout)
LOG.addHandler(SH)

S3_ENDPOINT = 'https://your-wos-endpoint-hostname-here' # WOS
#S3_ENDPOINT = 'http://127.0.0.1:9000' # Minio
S3_TAG = 'wos_testsuite-'

def get_s3_config():
    "Return the S3 config (access/secret access key) available in boto config."
    session = botocore.session.Session()
    if not session.profile in session.full_config['profiles']:
        return session.full_config['profiles']['default']
    return session.full_config['profiles'][session.profile]

def random_bytes(min_bytes=25 * 1024**2, max_bytes=50 * 1024**2):
    "Generate a byte stream with random contents, and return the file handle"
    file_size = random.randint(min_bytes, max_bytes)
    fh = io.BufferedRandom(io.BytesIO(os.urandom(file_size)))
    return fh

def random_string(length=13):
    "Return a random string of given length"
    random.seed()
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])

def s3_bucket_name(prefix='test-'):
    """
    Return a valid S3 bucket name, that follows AWS conventions
    (http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html)
    """
    min_length = 3
    max_length = 63 - len(prefix)
    return prefix + random_string(random.randint(min_length, max_length)).lower()

def tree_to_xml(tree):
    """
    Return an xml string representing the tree of element pairs.
    The tree looks like: [(tag, value), ...]
    where value could be a string or tree.
    """
    text = ''
    for (tag, value) in tree:
        text += '<' + tag + '>'
        if isinstance(value, (list, tuple)):
            text += tree_to_xml(value)
        else:
            text += str(value)
        text += '</' + tag + '>'

    return text

class TestWOSMPUploadXML(unittest.TestCase):
    "Test WOS S3 acceptance of MultiPart Upload XML payloads"

    @classmethod
    def setUpClass(self):
        self.fh = random_bytes()
        self.s3_client = boto3.client('s3', endpoint_url=S3_ENDPOINT, verify=False, config=botocore.config.Config(signature_version='s3'))
        self.s3 = boto3.resource('s3', endpoint_url=S3_ENDPOINT, verify=False, config=botocore.config.Config(signature_version='s3'))

        self.s3_bucket_name = s3_bucket_name()
        LOG.debug('Test bucket name is: {0}'.format(self.s3_bucket_name))
        self.s3_client.create_bucket(Bucket=self.s3_bucket_name)

    @classmethod
    def tearDownClass(self):
        self.fh.close()
        LOG.debug('Removing test bucket {0}'.format(self.s3_bucket_name))
        self.s3_client.delete_bucket(Bucket=self.s3_bucket_name)

    def setUp(self):
        self.fh.seek(0)
        self.key = S3_TAG + random_string()
        self.upload_id = None

    def tearDown(self):
        if self.upload_id is not None:
            response = self.s3_client.list_multipart_uploads(Bucket=self.s3_bucket_name, Prefix=self.key)
            if ('Uploads' in response and
                self.upload_id in [u['UploadId'] for u in response['Uploads']]):
                LOG.debug('Aborting upload {0}'.format(self.upload_id))
                self.s3_client.abort_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key, UploadId=self.upload_id)

        try:
          self.s3_client.head_object(Bucket=self.s3_bucket_name, Key=self.key)
        except ClientError as err:
            if err.response['Error']['Message'] != 'Not Found':
                raise
        else:
            self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=self.key)

    # MP Upload is implicit because we are ensuring that the minimum size of the object
    # is larger than the multipart chunksize
    def test_1_mpupload(self):
        "boto3 multi-part upload"
        s3_obj = self.s3.Object(self.s3_bucket_name, self.key)
        s3_obj.upload_fileobj(self.fh)

        # Verify that object exists (this does an implicit HEAD operation)
        s3_obj.reload()

    # This mimics the XML payload that Goofys uses for multi-part uploads
    def test_2_unordered_mpupload(self):
        "unordered xml-element multi-part upload"
        chunk_size = TransferConfig().multipart_chunksize
        num_parts = int(math.ceil(len(self.fh.read()) / chunk_size))
        self.fh.seek(0)
        response = self.s3_client.create_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key)
        self.upload_id = response['UploadId']
        LOG.debug('Created multipart upload {0}'.format(self.upload_id))
        uploaded_parts = []
        while len(uploaded_parts) <= num_parts:
            data = self.fh.read(chunk_size)
            if not data:
                # Reached end of file; No data left.
                break
            # Upload chunk
            part_number = len(uploaded_parts) + 1
            data_chksum = base64.standard_b64encode(hashlib.md5(data).digest()).decode('utf-8')
            response = self.s3_client.upload_part(Body=data,
                                                  Bucket=self.s3_bucket_name,
                                                  ContentLength=len(data),
                                                  ContentMD5=data_chksum,
                                                  Key=self.key,
                                                  PartNumber=part_number,
                                                  UploadId=self.upload_id)
            uploaded_parts.append(response['ETag'])

        parts = []
        for (index, etag) in enumerate(uploaded_parts):
            # Goofys doesn't keep consistent order of elements within each Part element,
            # so we mimic that behaviour here.
            part_attrib = (('PartNumber', index + 1),
                           ('ETag', etag))
            randomized_part = tuple(random.sample(part_attrib, k=len(part_attrib)))
            parts.append(('Part', randomized_part))
        tree = ("CompleteMultiPartUpload", parts)
        xml = tree_to_xml([tree])
        xml = '<?xml version="1.0"?>' + xml
        LOG.debug('Using xml payload:\n%s', xml)

        # Get signature for custom CompleteMultipartUpload request
        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}/{key}?uploadId={up_id}'.format(endpoint=S3_ENDPOINT,
                                                                  bucket=self.s3_bucket_name,
                                                                  key=self.key,
                                                                  up_id=self.upload_id)
        req = AWSRequest()
        req.method = 'POST'
        req.url = url
        s3_config = get_s3_config()
        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(req)

        # Use signature with custom CompleteMultipartUpload request
        resp = requests.post(url, headers=req.headers, data=xml.encode('utf-8'), verify=False)
        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()

    # This test is identical to the unordered one, except that 'Part' elements in the
    # xml always have the same order.
    def test_3_ordered_mpupload(self):
        "ordered xml-element multi-part upload"
        chunk_size = TransferConfig().multipart_chunksize
        num_parts = int(math.ceil(len(self.fh.read()) / chunk_size))
        self.fh.seek(0)
        response = self.s3_client.create_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key)
        self.upload_id = response['UploadId']
        LOG.debug('Created multipart upload {0}'.format(self.upload_id))
        uploaded_parts = []
        while len(uploaded_parts) <= num_parts:
            data = self.fh.read(chunk_size)
            if not data:
                # Reached end of file; No data left.
                break
            # Upload chunk
            part_number = len(uploaded_parts) + 1
            data_chksum = base64.standard_b64encode(hashlib.md5(data).digest()).decode('utf-8')
            response = self.s3_client.upload_part(Body=data,
                                                  Bucket=self.s3_bucket_name,
                                                  ContentLength=len(data),
                                                  ContentMD5=data_chksum,
                                                  Key=self.key,
                                                  PartNumber=part_number,
                                                  UploadId=self.upload_id)
            uploaded_parts.append(response['ETag'])

        parts = []
        for (index, etag) in enumerate(uploaded_parts):
            # Goofys doesn't keep consistent order of elements within each Part element,
            # so we mimic that behaviour here.
            part_attrib = (('PartNumber', index + 1),
                           ('ETag', etag))
            parts.append(('Part', part_attrib))
        tree = ("CompleteMultiPartUpload", parts)
        xml = tree_to_xml([tree])
        xml = '<?xml version="1.0"?>' + xml
        LOG.debug('Using xml payload:\n%s', xml)

        # Get signature for custom CompleteMultipartUpload request
        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}/{key}?uploadId={up_id}'.format(endpoint=S3_ENDPOINT,
                                                                  bucket=self.s3_bucket_name,
                                                                  key=self.key,
                                                                  up_id=self.upload_id)
        req = AWSRequest()
        req.method = 'POST'
        req.url = url
        s3_config = get_s3_config()
        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(req)

        # Use signature with custom CompleteMultipartUpload request
        resp = requests.post(url, headers=req.headers, data=xml.encode('utf-8'), verify=False)
        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()

    # This test is identical to the unordered mutli-part upload,
    # except that it escapes double-quote marks with: &#34;
    def test_4_unordered_escaped_mpupload(self):
        "unordered, escaped xml-element multi-part upload"
        chunk_size = TransferConfig().multipart_chunksize
        num_parts = int(math.ceil(len(self.fh.read()) / chunk_size))
        self.fh.seek(0)
        response = self.s3_client.create_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key)
        self.upload_id = response['UploadId']
        LOG.debug('Created multipart upload {0}'.format(self.upload_id))
        uploaded_parts = []
        while len(uploaded_parts) <= num_parts:
            data = self.fh.read(chunk_size)
            if not data:
                # Reached end of file; No data left.
                break
            # Upload chunk
            part_number = len(uploaded_parts) + 1
            data_chksum = base64.standard_b64encode(hashlib.md5(data).digest()).decode('utf-8')
            response = self.s3_client.upload_part(Body=data,
                                                  Bucket=self.s3_bucket_name,
                                                  ContentLength=len(data),
                                                  ContentMD5=data_chksum,
                                                  Key=self.key,
                                                  PartNumber=part_number,
                                                  UploadId=self.upload_id)
            uploaded_parts.append(response['ETag'])

        parts = []
        for (index, etag) in enumerate(uploaded_parts):
            # Goofys doesn't keep consistent order of elements within each Part element,
            # so we mimic that behaviour here.
            part_attrib = (('PartNumber', index + 1),
                           ('ETag', etag))
            randomized_part = tuple(random.sample(part_attrib, k=len(part_attrib)))
            parts.append(('Part', randomized_part))
        tree = ("CompleteMultiPartUpload", parts)
        xml = tree_to_xml([tree])
        xml = re.sub(r'"', '&#34;', xml)
        xml = '<?xml version="1.0"?>' + xml
        LOG.debug('Using xml payload:\n%s', xml)

        # Get signature for custom CompleteMultipartUpload request
        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}/{key}?uploadId={up_id}'.format(endpoint=S3_ENDPOINT,
                                                                  bucket=self.s3_bucket_name,
                                                                  key=self.key,
                                                                  up_id=self.upload_id)
        req = AWSRequest()
        req.method = 'POST'
        req.url = url
        s3_config = get_s3_config()
        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(req)

        # Use signature with custom CompleteMultipartUpload request
        resp = requests.post(url, headers=req.headers, data=xml.encode('utf-8'), verify=False)
        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()

    # This test is identical to the ordered mutli-part upload,
    # except that it escapes double-quote marks with: &#34;
    def test_5_ordered_escaped_mpupload(self):
        "ordered, escaped xml-element multi-part upload"
        chunk_size = TransferConfig().multipart_chunksize
        num_parts = int(math.ceil(len(self.fh.read()) / chunk_size))
        self.fh.seek(0)
        response = self.s3_client.create_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key)
        self.upload_id = response['UploadId']
        LOG.debug('Created multipart upload {0}'.format(self.upload_id))
        uploaded_parts = []
        while len(uploaded_parts) <= num_parts:
            data = self.fh.read(chunk_size)
            if not data:
                # Reached end of file; No data left.
                break
            # Upload chunk
            part_number = len(uploaded_parts) + 1
            data_chksum = base64.standard_b64encode(hashlib.md5(data).digest()).decode('utf-8')
            response = self.s3_client.upload_part(Body=data,
                                                  Bucket=self.s3_bucket_name,
                                                  ContentLength=len(data),
                                                  ContentMD5=data_chksum,
                                                  Key=self.key,
                                                  PartNumber=part_number,
                                                  UploadId=self.upload_id)
            uploaded_parts.append(response['ETag'])

        parts = []
        for (index, etag) in enumerate(uploaded_parts):
            # Goofys doesn't keep consistent order of elements within each Part element,
            # so we mimic that behaviour here.
            part_attrib = (('PartNumber', index + 1),
                           ('ETag', etag))
            parts.append(('Part', part_attrib))
        tree = ("CompleteMultiPartUpload", parts)
        xml = tree_to_xml([tree])
        xml = re.sub(r'"', '&#34;', xml)
        xml = '<?xml version="1.0"?>' + xml
        LOG.debug('Using xml payload:\n%s', xml)

        # Get signature for custom CompleteMultipartUpload request
        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}/{key}?uploadId={up_id}'.format(endpoint=S3_ENDPOINT,
                                                                  bucket=self.s3_bucket_name,
                                                                  key=self.key,
                                                                  up_id=self.upload_id)
        req = AWSRequest()
        req.method = 'POST'
        req.url = url
        s3_config = get_s3_config()
        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(req)

        # Use signature with custom CompleteMultipartUpload request
        resp = requests.post(url, headers=req.headers, data=xml.encode('utf-8'), verify=False)
        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()

    def test_6_unordered_nohead_mpupload(self):
        "unordered xml-element without xml header multi-part upload"
        chunk_size = TransferConfig().multipart_chunksize
        num_parts = int(math.ceil(len(self.fh.read()) / chunk_size))
        self.fh.seek(0)
        response = self.s3_client.create_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key)
        self.upload_id = response['UploadId']
        LOG.debug('Created multipart upload {0}'.format(self.upload_id))
        uploaded_parts = []
        while len(uploaded_parts) <= num_parts:
            data = self.fh.read(chunk_size)
            if not data:
                # Reached end of file; No data left.
                break
            # Upload chunk
            part_number = len(uploaded_parts) + 1
            data_chksum = base64.standard_b64encode(hashlib.md5(data).digest()).decode('utf-8')
            response = self.s3_client.upload_part(Body=data,
                                                  Bucket=self.s3_bucket_name,
                                                  ContentLength=len(data),
                                                  ContentMD5=data_chksum,
                                                  Key=self.key,
                                                  PartNumber=part_number,
                                                  UploadId=self.upload_id)
            uploaded_parts.append(response['ETag'])

        parts = []
        for (index, etag) in enumerate(uploaded_parts):
            # Goofys doesn't keep consistent order of elements within each Part element,
            # so we mimic that behaviour here.
            part_attrib = (('PartNumber', index + 1),
                           ('ETag', etag))
            randomized_part = tuple(random.sample(part_attrib, k=len(part_attrib)))
            parts.append(('Part', randomized_part))
        tree = ("CompleteMultiPartUpload", parts)
        xml = tree_to_xml([tree])
        LOG.debug('Using xml payload:\n%s', xml)

        # Get signature for custom CompleteMultipartUpload request
        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}/{key}?uploadId={up_id}'.format(endpoint=S3_ENDPOINT,
                                                                  bucket=self.s3_bucket_name,
                                                                  key=self.key,
                                                                  up_id=self.upload_id)
        req = AWSRequest()
        req.method = 'POST'
        req.url = url
        s3_config = get_s3_config()
        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(req)

        # Use signature with custom CompleteMultipartUpload request
        resp = requests.post(url, headers=req.headers, data=xml.encode('utf-8'), verify=False)
        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()

    def test_7_unordered_escaped_nohead_mpupload(self):
        "unordered, escaped xml-element without xml header multi-part upload"
        chunk_size = TransferConfig().multipart_chunksize
        num_parts = int(math.ceil(len(self.fh.read()) / chunk_size))
        self.fh.seek(0)
        response = self.s3_client.create_multipart_upload(Bucket=self.s3_bucket_name, Key=self.key)
        self.upload_id = response['UploadId']
        LOG.debug('Created multipart upload {0}'.format(self.upload_id))
        uploaded_parts = []
        while len(uploaded_parts) <= num_parts:
            data = self.fh.read(chunk_size)
            if not data:
                # Reached end of file; No data left.
                break
            # Upload chunk
            part_number = len(uploaded_parts) + 1
            data_chksum = base64.standard_b64encode(hashlib.md5(data).digest()).decode('utf-8')
            response = self.s3_client.upload_part(Body=data,
                                                  Bucket=self.s3_bucket_name,
                                                  ContentLength=len(data),
                                                  ContentMD5=data_chksum,
                                                  Key=self.key,
                                                  PartNumber=part_number,
                                                  UploadId=self.upload_id)
            uploaded_parts.append(response['ETag'])

        parts = []
        for (index, etag) in enumerate(uploaded_parts):
            # Goofys doesn't keep consistent order of elements within each Part element,
            # so we mimic that behaviour here.
            part_attrib = (('PartNumber', index + 1),
                           ('ETag', etag))
            randomized_part = tuple(random.sample(part_attrib, k=len(part_attrib)))
            parts.append(('Part', randomized_part))
        tree = ("CompleteMultiPartUpload", parts)
        xml = tree_to_xml([tree])
        xml = re.sub(r'"', '&#34;', xml)
        LOG.debug('Using xml payload:\n%s', xml)

        # Get signature for custom CompleteMultipartUpload request
        s3_config = get_s3_config()
        url = '{endpoint}/{bucket}/{key}?uploadId={up_id}'.format(endpoint=S3_ENDPOINT,
                                                                  bucket=self.s3_bucket_name,
                                                                  key=self.key,
                                                                  up_id=self.upload_id)
        req = AWSRequest()
        req.method = 'POST'
        req.url = url
        s3_config = get_s3_config()
        creds = botocore.credentials.Credentials(access_key=s3_config['aws_access_key_id'], secret_key=s3_config['aws_secret_access_key'])
        auth = botocore.auth.HmacV1Auth(creds)
        auth.add_auth(req)

        # Use signature with custom CompleteMultipartUpload request
        resp = requests.post(url, headers=req.headers, data=xml.encode('utf-8'), verify=False)
        if resp.status_code != requests.codes.ok:
            LOG.debug('S3 Response:\n%s', resp.text)
        resp.connection.close()
        resp.raise_for_status()

if __name__ == '__main__':
    unittest.main(verbosity=2)
