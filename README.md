# s3_helpers

## rand_pos_test.py

This script tests read-time at beginning/middle/end of objects, for an S3 endpoint.

1. Install the python requirements for the `rand_pos_test.py` script, from `requirements.txt`.
Ex:
```
# In a virtualenv
pip install -r requirements.txt
```
2. Set the `S3_ENDPOINT`, `TEMP_DIR` variables in the `rand_pos_test.py` script
3. Run the `rand_pos_test.py` script.

Test results are available in the `rand_pos_test_results.html` and `rand_pos_test_stats.json` files.
